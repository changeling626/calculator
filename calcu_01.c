#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define INST_NUM 12              // put the number of instructions here

char* table [INST_NUM][3];
char* dupTable [INST_NUM][3];
int regist[10];

int distNum (char* num);
int lineEx (int i);

int main(void) {

        FILE* inst_reg;
        size_t len = 0;
        ssize_t lineread;
        int row = 0;
        int col = 0;
        char* buffer;
        char* tokbuff;
        char operator;
        int operand1, operand2;

//        fstream = fopen("input.txt", "r");  // error occurs
        inst_reg = fopen("/Users/hyewonpark/Documents/calculator/test.txt", "r");

        while ((lineread = getline(&buffer, &len, inst_reg)) != -1) {
                
                tokbuff = strtok(buffer, " \n");
                while(tokbuff != NULL) {
                        table[row][col] = tokbuff;
                        dupTable[row][col] = strdup(tokbuff);
                        col++;
                        tokbuff = strtok(NULL, " \n");
                }
                col = 0;
                row++;
        }

        int test = 0;
        int k = 0;
        while (test < INST_NUM) {
                while (k < INST_NUM) {
                        test = lineEx(k);
                        if (test != 0) {
                                break;
                        }
                        k++;
                }
                if (k == INST_NUM) break;
                else k = k + INST_NUM;
                lineEx(test);
                test++;
        }

        return 0;

}

int lineEx (int i) {

    int oprnd1, oprnd2;

    if ((strcmp(dupTable[i][0], "+")) == 0) {
            oprnd1 = distNum(dupTable[i][1]);
            oprnd2 = distNum(dupTable[i][2]);
            regist[0] = oprnd1 + oprnd2;
            printf("line %d >> %s %s %s -> ", i, dupTable[i][0], dupTable[i][1], dupTable[i][2]);
            printf("R0 : %d = %d + %d\n", regist[0], oprnd1, oprnd2);
            return 0;
    }
    else if ((strcmp(dupTable[i][0], "-")) == 0) {
            oprnd1 = distNum(dupTable[i][1]);
            oprnd2 = distNum(dupTable[i][2]);
            regist[0] = oprnd1 - oprnd2;
            printf("line %d >> %s %s %s -> ", i, dupTable[i][0], dupTable[i][1], dupTable[i][2]);
            printf("R0 : %d = %d - %d\n", regist[0], oprnd1, oprnd2);
            return 0;
    }
    else if ((strcmp(dupTable[i][0], "*")) == 0) {
            oprnd1 = distNum(dupTable[i][1]);
            oprnd2 = distNum(dupTable[i][2]);
            regist[0] = oprnd1 * oprnd2;
            printf("line %d >> %s %s %s -> ", i, dupTable[i][0], dupTable[i][1], dupTable[i][2]);
            printf("R0 : %d = %d * %d\n", regist[0], oprnd1, oprnd2);
            return 0;
    }
    else if ((strcmp(dupTable[i][0], "/")) == 0) {
            oprnd1 = distNum(dupTable[i][1]);
            oprnd2 = distNum(dupTable[i][2]);
            if (oprnd2 == 0) {
                printf("Error occurred. You can't divide a number with 0.\n"); exit(0);
            }
            else;
            regist[0] = oprnd1 / oprnd2;
            printf("line %d >> %s %s %s -> ", i, dupTable[i][0], dupTable[i][1], dupTable[i][2]);
            printf("R0 : %d = %d / %d\n", regist[0], oprnd1, oprnd2);
            return 0;
    }
    else if (strcmp(dupTable[i][0], "M") == 0) {
            char dist1 = *dupTable[i][1];
            char dist2 = *dupTable[i][2];
            if (dist1 == dist2 && dist1 == 'R') {
                    if (*dupTable[i][1] != 'R' || *dupTable[i][2] != 'R') {
                            printf("Error occured. At least one operand should be Register value.\n"); exit(0);
                    }
                    else;
                    oprnd1 = (int)strtol(dupTable[i][1]+1, NULL, 10);
                    oprnd2 = (int)strtol(dupTable[i][2]+1, NULL, 10);
                    regist[oprnd2] = regist[oprnd1];
                    printf("line %d >> %s %s %s -> ", i, dupTable[i][0], dupTable[i][1], dupTable[i][2]);
                    printf("R%d : %d\n", oprnd2, regist[0]);
                    return 0;
            }
            else {
                    int oprnd1 = (int)strtol(dupTable[i][1]+1, NULL, 10);
                    oprnd2 = distNum(dupTable[i][2]);
                    regist[oprnd1] = oprnd2;
                    printf("line %d >> %s %s %s -> ", i, dupTable[i][0], dupTable[i][1], dupTable[i][2]);
                    printf("R%d : %d\n", oprnd1, regist[oprnd1]);
                    return 0;
            }
    }
    else if (strcmp(dupTable[i][0], "C") == 0) {
            if (*dupTable[i][1] != *dupTable[i][2] || *dupTable[i][1] != 'R') {
            printf("Error occured. Only Register values should be put as operands.\n"); exit(0);
            }
            else;
            oprnd1 = (int)strtol(dupTable[i][1]+1, NULL, 10);
            oprnd2 = (int)strtol(dupTable[i][2]+1, NULL, 10);
            if (regist[oprnd1] > regist[oprnd2]) regist[0] = 1;
            else if (regist[oprnd1] < regist[oprnd2]) regist[0] = -1;
            else regist[0] = 0;
            printf("line %d >> %s %s %s -> ", i, dupTable[i][0], dupTable[i][1], dupTable[i][2]);
            printf("R0 : %d\n", regist[0]);
            return 0;
    }
    else if (strcmp(dupTable[i][0], "J") == 0) {
            oprnd1 = distNum(dupTable[i][1]);
            return oprnd1;
    }
    else if (strcmp(dupTable[i][0], "BEQ") == 0) {
            oprnd1 = distNum(dupTable[i][1]);
            if (regist[0] == 0) return oprnd1;
            else return 0;
    }
    else {
            printf("Error occurred.\n");
            exit(0);
    }
}

int distNum (char* num) {

        char dist1 = *num;
        char dist2 = *(num+1);
        int re = 0;
        if (dist1 == '0' && dist2 == 'x') {
                re = (int)strtol(num+2, NULL, 16);
                return re;
        }
        else if (dist1 == 'R') {
                re = (int)strtol(num+1, NULL, 10);
                return regist[re];
        }
        else {
                re = (int)strtol(num, NULL, 10);
                return re;
        }
}